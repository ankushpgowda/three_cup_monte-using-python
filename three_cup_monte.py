cups = [1, 2, 3]
ans_cup = 2
positions = {'l':0, 'c': 1, 'r':2}
game_status = True
from random import shuffle

def do_shuffle(cups):
    return shuffle(cups)

def check_user_choice(user_choice):
    if(positions[user_choice] == cups.index(ans_cup)):
        return True
    else: 
        return False

while game_status:
    game_status_temp = input("Enter 's' to Shuffle or 'e' to Exit the Game")
    if game_status_temp.lower() == 's':
        do_shuffle(cups)
        user_choice = input("Enter position of the cup to be picked(1.For left enter 'l', 2.For center enter 'c', 3.For right enter 'r')")
        bool_value = check_user_choice(user_choice)
        if bool_value == True:
            print("YOUR GUESS IS CORRECT !!")
        else:
            print("YOUR GUESS IS WRONG :-(")
        ans_cup = cups[1]
        game_status = True
    elif game_status_temp.lower() == 'e':
        game_status = False
    else:
        print("Choose proper options")